#!/usr/bin/python3
# -*- coding: utf-8 -*-

# =====================================================================
#               GENERADOR DE DIAGRAMAS UML
#
# Autor:  Franyer Hidalgo - Venezuela (fhidalgo.dev@gmail.com)
# Fecha:  Mayo-Junio 2020
#
# Este script permite hacer ingeniería inversa a un models.py de odoo,
# convirtiendo una clase de Odoo en un diagrama de clase UML.
# =====================================================================

# Se importa el módulo os para realizar operaciones en el sistema operativo
import os


class Presentacion:
    """ Permite presentar mensajes al usuario para una mejor UX """

    def __init__(self, lenguaje, framework=''):
        self.lenguaje_programacion = lenguaje
        self.framework = framework
        self.colaboradores = ['FranyerH - VE']

    def formatear_mensaje(self, decoracion, msg):
        """ Formatear un mensaje colocandole un header y un footer según su decoración """
        decoracion = decoracion * 80
        mensaje = """"""

        mensaje += """{0}\n\n{1}\n\n{0}""".format(decoracion, msg)
        return mensaje

    def bienvenido(self):
        """ Mostrar mensaje de bienvenida """
        msg = "\t\tBIENVENIDOS AL GENERADOR DE INGENERÍA INVERSA UML\n"
        msg += "\nAnalizará las clases de la siguiente tecnología:"
        msg += "\n-Lenguaje de programación: {}".format(self.lenguaje_programacion)
        if self.framework:
            msg += "\n-Framework: {}".format(self.framework)
        print(self.formatear_mensaje('=', msg))

    def despedida(self):
        msg = "\n================================================================================"
        msg += "\n\t\tGRACIAS POR USAR EL GENERADOR UML\n"
        msg += "\n-Créditos: {}".format((',').join(self.colaboradores))
        print(msg)


class Archivo:
    """ Permite el manejo de archivos """
    def __init__(self, name, modo_apertura='r'):
        self.name = name
        self.check = bool
        self.archivo = object
        self.modo_apertura = modo_apertura

    def chequear(self):
        """ Comprobar que existe el archivo a manejar """
        try:
            self.archivo = open(self.name, self.modo_apertura)
        except OSError:
            print("No se puede abrir el archivo {}".format(self.name))
            self.check = False
        else:
            print("Archivo {} abierto satisfactoriamente".format(self.name))
            self.archivo.close()
            self.check = True

    def crear_archivo(self):
        """ Crear un nuevo archivo sin cerrarlo """
        self.archivo = open(self.name, self.modo_apertura)
        return self.archivo


class DirectorioModulo:
    """ Permite el manejo de una carpeta con sus sub-carpetas y visualización
        de archivos dentro de ellas
    """
    def __init__(self, name):
        self.name = name
        self.check = bool

    def chequear(self, carpeta):
        """ Comprobar la existencia de una carpeta """
        self.check = os.path.exists(carpeta)
        if self.check:
            print("La carpeta \"{}\" se encuentra actualmente "
                  "disponible en este directorio".format(carpeta))
        else:
            print("La carpeta \"{}\" no existe en este directorio".format(carpeta))
            exit()

    def ls_modulo(self):
        """ Listar directorio del módulo """
        directorio = os.listdir(self.name)
        return directorio

    def find_subdir(self, subdir):
        """ Buscar sub-directorios dentro del módulo """
        if subdir in self.ls_modulo():
            return os.listdir('{}/{}'.format(self.name, subdir))
        else:
            print("La carpeta {} no existe dentro del sub-directorio {}".format(subdir, self.name))
            exit()

    def mostrar_subdir(self, directorio):
        """ Mostrar sub-directorios """
        print("\nEl sub-directorio de la carpeta {} es el siguiente:".format(self.name))
        for i in directorio:
            print("  -{}".format(i))


class OdooModels:
    """ Gestiona el análisis de los models.py de un módulo Odoo """

    TIPO_FIELDS = [
        'fields.Char',
        'fields.Selection',
        'fields.Html',
        'fields.Text',
        'fields.Date',
        'fields.Datetime',
        'fields.Integer',
        'fields.Float',
        'fields.Binary',
        'fields.Boolean',
        'fields.Many2one',
        'fields.One2many',
        'fields.Many2many',
        'fields.Reference',
    ]

    # Guarda el formato del field como: "fields.tipo(" para evitar lineas filtradas en el recorrido de archivos
    FORMATO_FIELD = ['{}('.format(i) for i in TIPO_FIELDS]

    def __init__(self):
        self.modulo = object
        self.archivo = object
        self.models = []
        self.extensiones = ['.py']
        self.excepciones = ['__init__.py']
        self._name = []  # Lista de los _name
        self.clases = []  # Lista del nombre de la clases
        # Dict = nombre_clase: {nombre_field: {tipo_field:'', valor_field:''}}
        self.clases_atributos = {}

    def solicitar_modulo(self):
        """ Chequear si la carpeta del módulo existe """
        modulo = input('\nIngrese el módulo de odoo que desea analizar: ').strip()
        self.modulo = DirectorioModulo(modulo)
        self.modulo.chequear(modulo)
        return self.modulo.check

    def extraer_clases(self, models):
        """ Extraer todas las clases dentro de un models.py de odoo, estructurando sus:
            -Nombre de fields
            -Tipo de fields
        """
        with open(models) as archivo:
            # Booleano que indica que solo se analicen fields de clases que hereden de models.Model
            models_Model = False
            for linea in archivo:
                # Busca todas las concidencias de "_name" en cada línea entre el índice 4:9
                if len(linea) >= 8 and linea[4:9] == '_name':
                    self._name.append(linea.split(" ")[-1].replace('\n', '').replace('\'', '').replace('"', ''))

                # Busca todas las concidencias del string "(models.Model):" en cada línea
                if linea.find("(models.Model):") != -1:
                    self.clases.append(linea.split(" ")[-1].replace("(models.Model):\n", ""))
                    self.clases_atributos.update({self.clases[-1]: {}})
                    models_Model = True
                elif linea.find("(models.TransientModel):") != -1:
                    models_Model = False

                # Recorrer el FORMATO_FIELD para encontrar coincidencias por línea
                for formato in self.FORMATO_FIELD:
                    # Busca todas las concidencias de cada formato_field en cada línea
                    if linea.find(formato) != -1 and models_Model:
                        # Nombre del field
                        nombre_field = linea[4:].replace('\n', '').split("fields")[0].replace(' ', '').replace('=', '')
                        # Tipo de dato del field
                        tipo_field = formato.lstrip("fields.").rstrip('(')
                        # Contenido preliminar del valor_field
                        valor_field = linea[4:].replace('\n', '').split("fields")[-1]

                        # Estableciendo el valor_field sin el primer parentesis
                        if valor_field.find(formato[6:]) != -1:
                            # Validar cuando exista un salto de linea al abrir el parentesis del valor del field
                            if valor_field == formato[6:]:
                                valor_field = archivo.readline().replace('\n', '').strip()
                            # Reemplaza el .TipoDato en la linea analizada
                            valor_field = valor_field.replace(formato[6:], '')

                        # Evalúar que el cierre del field sea el correspondiente
                        while valor_field[-1] != ')' or valor_field[0] == '[':
                            linea_next = archivo.readline().replace('\n', '').strip()
                            valor_field += ' ' + linea_next
                            if valor_field[-1] == ')' and linea_next[0] != '(':
                                break

                        # Quitar el último parentesis al field
                        valor_field = valor_field[:-1]

                        # Actualizar el nombre de la clase con sus atributos y valores
                        self.clases_atributos[self.clases[-1]].update({
                            nombre_field: {  # Nombre del field
                                'tipo': tipo_field,  # Tipo del field
                                'valor': valor_field  # Valor del field
                            }
                        })

    def analizar_models(self, model):
        self.archivo = Archivo(model)
        # Guardar el nombre del archivo desde su ruta
        self.archivo.name = os.path.join(self.modulo.name, 'models', model)
        self.archivo.chequear()

    def ensamblar_generador(self):
        self.solicitar_modulo()
        # Archivos dentro de la carpeta models
        self.models = self.modulo.find_subdir('models')
        # Filtrar todos los models con las extenciones y excepciones correspondientes
        self.models = [
            model
            for model in self.models
            if model[-3:] in self.extensiones and model not in self.excepciones
        ]


class GeneradorUML(OdooModels):
    """Gestiona el generador UML en la herramienta dia"""

    def get_cabecera(self):
        """Cabecera xml del archivo .dia"""
        return """<?xml version="1.0" encoding="UTF-8"?>
<dia:diagram xmlns:dia="http://www.lysator.liu.se/~alla/dia/">

    <dia:diagramdata>
        <dia:attribute name="background">
            <dia:color val="#ffffffff"/>
        </dia:attribute>
        <dia:attribute name="pagebreak">
            <dia:color val="#000099ff"/>
        </dia:attribute>
        <dia:attribute name="paper">
            <dia:composite type="paper">
                <dia:attribute name="name">
                    <dia:string>#A4#</dia:string>
                </dia:attribute>
                <dia:attribute name="tmargin">
                    <dia:real val="1.2999999523162842"/>
                </dia:attribute>
                <dia:attribute name="bmargin">
                    <dia:real val="1.2999999523162842"/>
                </dia:attribute>
                <dia:attribute name="lmargin">
                    <dia:real val="1"/>
                </dia:attribute>
                <dia:attribute name="rmargin">
                    <dia:real val="1"/>
                </dia:attribute>
                <dia:attribute name="is_portrait">
                    <dia:boolean val="false"/>
                </dia:attribute>
                <dia:attribute name="scaling">
                    <dia:real val="0.34999999403953552"/>
                </dia:attribute>
                <dia:attribute name="fitto">
                    <dia:boolean val="false"/>
                </dia:attribute>
            </dia:composite>
        </dia:attribute>
        <dia:attribute name="grid">
            <dia:composite type="grid">
                <dia:attribute name="dynamic">
                    <dia:boolean val="true"/>
                </dia:attribute>
                <dia:attribute name="width_x">
                    <dia:real val="1"/>
                </dia:attribute>
                <dia:attribute name="width_y">
                    <dia:real val="1"/>
                </dia:attribute>
                <dia:attribute name="visible_x">
                    <dia:int val="1"/>
                </dia:attribute>
                <dia:attribute name="visible_y">
                    <dia:int val="1"/>
                </dia:attribute>
            <dia:composite type="color"/>
            </dia:composite>
        </dia:attribute>
        <dia:attribute name="color">
            <dia:color val="#d8e5e5ff"/>
        </dia:attribute>
        <dia:attribute name="guides">
            <dia:composite type="guides">
                <dia:attribute name="hguides"/>
                <dia:attribute name="vguides"/>
            </dia:composite>
        </dia:attribute>
        <dia:attribute name="display">
            <dia:composite type="display">
                <dia:attribute name="antialiased">
                    <dia:boolean val="false"/>
                </dia:attribute>
                <dia:attribute name="snap-to-grid">
                    <dia:boolean val="true"/>
                </dia:attribute>
                <dia:attribute name="snap-to-object">
                    <dia:boolean val="true"/>
                </dia:attribute>
                <dia:attribute name="show-grid">
                    <dia:boolean val="true"/>
                </dia:attribute>
                <dia:attribute name="show-connection-points">
                    <dia:boolean val="true"/>
                </dia:attribute>
            </dia:composite>
        </dia:attribute>
    </dia:diagramdata>

    <dia:layer name="Background" visible="true" connectable="true" active="true">
"""

    def generar_clases(self, data):
        """Crear clases correspondientes en el archivo .dia"""
        result = """"""
        id = 0  # Se establece un id para cada dia:object type="UML - Class"

        # Se itera cada clase y su contenido
        for clase, contenido in data.items():
            id += 1
            result += """        <dia:object type="UML - Class" version="0" id="{0}">
            <dia:attribute name="obj_pos">
                <dia:point val="0,0"/>
            </dia:attribute>
            <dia:attribute name="obj_bb">
                <dia:rectangle val="14.95,26.95;27.485,48.85"/>
            </dia:attribute>
            <dia:attribute name="elem_corner">
                <dia:point val="0,0"/>
            </dia:attribute>
            <dia:attribute name="elem_width">
                <dia:real val="12.434999999999999"/>
            </dia:attribute>
            <dia:attribute name="elem_height">
                <dia:real val="21.800000000000004"/>
            </dia:attribute>
            <dia:attribute name="name">
                <dia:string>#{1}#</dia:string>
            </dia:attribute>
            <dia:attribute name="stereotype">
                <dia:string>##</dia:string>
            </dia:attribute>
            <dia:attribute name="comment">
                <dia:string>##</dia:string>
            </dia:attribute>
            <dia:attribute name="abstract">
                <dia:boolean val="false"/>
            </dia:attribute>
            <dia:attribute name="suppress_attributes">
                <dia:boolean val="false"/>
            </dia:attribute>
            <dia:attribute name="suppress_operations">
                <dia:boolean val="false"/>
            </dia:attribute>
            <dia:attribute name="visible_attributes">
                <dia:boolean val="true"/>
            </dia:attribute>
            <dia:attribute name="visible_operations">
                <dia:boolean val="true"/>
            </dia:attribute>
            <dia:attribute name="visible_comments">
                <dia:boolean val="false"/>
            </dia:attribute>
            <dia:attribute name="wrap_operations">
                <dia:boolean val="true"/>
            </dia:attribute>
            <dia:attribute name="wrap_after_char">
                <dia:int val="40"/>
            </dia:attribute>
            <dia:attribute name="comment_line_length">
                <dia:int val="120"/>
            </dia:attribute>
            <dia:attribute name="comment_tagging">
                <dia:boolean val="false"/>
            </dia:attribute>
            <dia:attribute name="allow_resizing">
                <dia:boolean val="false"/>
            </dia:attribute>
            <dia:attribute name="line_width">
                <dia:real val="0.10000000000000001"/>
            </dia:attribute>
            <dia:attribute name="line_color">
                <dia:color val="#000000ff"/>
            </dia:attribute>
            <dia:attribute name="fill_color">
                <dia:color val="#ffffffff"/>
            </dia:attribute>
            <dia:attribute name="text_color">
                <dia:color val="#000000ff"/>
            </dia:attribute>
            <dia:attribute name="normal_font">
                <dia:font family="monospace" style="0" name="Courier"/>
            </dia:attribute>
            <dia:attribute name="abstract_font">
                <dia:font family="monospace" style="88" name="Courier-BoldOblique"/>
            </dia:attribute>
            <dia:attribute name="polymorphic_font">
                <dia:font family="monospace" style="8" name="Courier-Oblique"/>
            </dia:attribute>
            <dia:attribute name="classname_font">
                <dia:font family="sans" style="80" name="Helvetica-Bold"/>
            </dia:attribute>
            <dia:attribute name="abstract_classname_font">
                <dia:font family="sans" style="88" name="Helvetica-BoldOblique"/>
            </dia:attribute>
            <dia:attribute name="comment_font">
                <dia:font family="sans" style="8" name="Helvetica-Oblique"/>
            </dia:attribute>
            <dia:attribute name="normal_font_height">
                <dia:real val="0.80000000000000004"/>
            </dia:attribute>
            <dia:attribute name="polymorphic_font_height">
                <dia:real val="0.80000000000000004"/>
            </dia:attribute>
            <dia:attribute name="abstract_font_height">
                <dia:real val="0.80000000000000004"/>
            </dia:attribute>
            <dia:attribute name="classname_font_height">
                <dia:real val="1"/>
            </dia:attribute>
            <dia:attribute name="abstract_classname_font_height">
                <dia:real val="1"/>
            </dia:attribute>
            <dia:attribute name="comment_font_height">
                <dia:real val="0.69999999999999996"/>
            </dia:attribute>
            <dia:attribute name="attributes">
""".format(id, clase)

        # Se itera cada field del contenido de la clase con su valor y tipo
            for field, vals_type in contenido.items():
                result += """
                <dia:composite type="umlattribute">
                    <dia:attribute name="name">
                        <dia:string>#{0}#</dia:string>
                    </dia:attribute>
                    <dia:attribute name="type">
                        <dia:string>#{1}#</dia:string>
                    </dia:attribute>
                    <dia:attribute name="value">
                        <dia:string>#{2}#</dia:string>
                    </dia:attribute>
                    <dia:attribute name="comment">
                        <dia:string>##</dia:string>
                    </dia:attribute>
                    <dia:attribute name="visibility">
                        <dia:enum val="0"/>
                    </dia:attribute>
                    <dia:attribute name="abstract">
                        <dia:boolean val="false"/>
                    </dia:attribute>
                    <dia:attribute name="class_scope">
                        <dia:boolean val="false"/>
                    </dia:attribute>
                </dia:composite>
""".format(field, vals_type['tipo'], vals_type['valor'])

            result += """
                </dia:attribute>
            </dia:object>
            """
        return result

    def get_final(self):
        return """
    </dia:layer>
</dia:diagram>
    """

    def menu(self):
        """ Menú de selección para crear carpetas """
        print ("\n¿Dónde desea crear los diagramas uml?")
        print ("\t1 - En un carpeta doc en el directorio actual")
        print ("\t2 - En una carpeta doc dentro del módulo")
        print ("\t0 - Salir")

    def ejecutar_menu(self):
        """ Tomar la entrada del usuario y ejecutar menú """
        ruta_uml = '/uml/class_diagram/'
        while True:
            self.menu()
            opcion_menu = input("Inserta una opción válida >> ").strip()
            if opcion_menu == "1":
                ruta_doc = 'doc_{}'.format(self.modulo.name)
                ruta_completa = '{}{}'.format(ruta_doc, ruta_uml)
                os.makedirs(ruta_completa, exist_ok=True)
                return ruta_completa
            elif opcion_menu == "2":
                ruta_doc = '{}/doc'.format(self.modulo.name)
                ruta_completa = '{}{}'.format(ruta_doc, ruta_uml)
                os.makedirs(ruta_completa, exist_ok=True)
                return ruta_completa
            elif opcion_menu == "0":
                exit()
            else:
                input("No has pulsado ninguna opción correcta...Pulsa una tecla para continuar")
        print('\n')

    def generar_archivo_dia(self, nombre_archivo):
        """Generar archivo .dia"""
        # Crear el archivo .dia tomando el nombre del modelo analizado
        uml_dia = Archivo(nombre_archivo, 'w').crear_archivo()
        # Escribir la cabecera del archivo .dia
        uml_dia.write(self.get_cabecera())
        # Escribir las clases UML dentro del archivo .dia
        uml_dia.write(self.generar_clases(self.clases_atributos))
        # Escribir el footer del archivo .dia
        uml_dia.write(self.get_final())
        # Cerrar el archivo .dia
        uml_dia.close()

    def generar_uml(self):
        """ Se hereda el ensamblaje del generador y se añaden los analisis
            de los archivos correspondientes a Odoo
        """
        super(GeneradorUML, self).ensamblar_generador()
        archivos_rechazados = []  # Archivos con problemas al abrir
        archivos_satisfactorios = []  # Archivos abiertos sin problemas
        archivos_generados = []  # Archivos dias generados
        archivos_no_generados = []  # Archivos dias no generados
        print('\nSerán analizados los siguientes modelos: {}'.format(self.models))
        ruta_doc = self.ejecutar_menu()  # Crear carpetas doc y almacenar ruta
        for model in self.models:
            self.analizar_models(model)
            if self.archivo.check:
                archivos_satisfactorios.append(model)  # Archivo abierto satisfactoriamente
                self.extraer_clases(self.archivo.name)  # Analizar el models.py
                if self.clases_atributos:  # Validar que el diccionario venga lleno
                    self.generar_archivo_dia('{}{}.dia'.format(ruta_doc, model[:-3]))
                    archivos_generados.append(model[:-3] + '.dia')
                else:
                    archivos_no_generados.append(model)
                self.clases_atributos = {}  # Vaciar Diccionario
            else:
                archivos_rechazados.append(model)

        ######################################
        #           Estadísticas             #
        ######################################
        print("\nLos archivos fueron creados en la siguiente ruta {}".format(ruta_doc))
        print("\
            \nFueron Analizados Satisfactoriamente {} Archivos \
            \nFueron Rechazados {} Archivos \
            ".format(len(archivos_satisfactorios), len(archivos_rechazados)))
        if archivos_rechazados:
            print("Dentro de los Archivos Rechazados están: {}".format(archivos_rechazados))

        print("\nSe crearon satisfactoriamente los siguientes diagramas de clase:"
              "\n-{}".format(('\n-').join(archivos_generados)))
        if archivos_no_generados:
            print("\nSin embargo, estos archivos no tienen clases heredadas de models.Model:"
                  "\n-{}"
                  "\nPor lo cual no se crearon los diagramas correspondientes".format(('\n-').join(archivos_no_generados)))


MENSAJE = Presentacion('Python', 'Odoo')
MENSAJE.bienvenido()
GeneradorUML().generar_uml()
MENSAJE.despedida()
